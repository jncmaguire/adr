# Use git for version control

## Status

Accepted

## Context

There are various different version control tools we can use as software engineers, and as software engineers, we must always use one.

## Decision

Use git.

## Consequences

From [git vs. svn]:
* faster to commit
* decentralized / no single point of failure
* available offline
* more complicated history logs

<!-- References -->
[git vs. svn]: https://backlog.com/blog/git-vs-svn-version-control-system/