# Record architecture decisions

## Status

Accepted

## Context

We need to record the architectural decisions made on this project.

## Decision

Use "Architecture Decision Records," as described by [Michael Nygard].

## Consequences

See [Michael Nygard].

<!-- References -->
[Michael Nygard]: http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions